﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class VRToggle : MonoBehaviour {


	public void Toggle()
	{
		Cardboard.SDK.VRModeEnabled = !Cardboard.SDK.VRModeEnabled;

	}
}
